package com.example.lesson5

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.junit.Test
import kotlin.concurrent.thread

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
//    @Test
//    fun addition_isCorrect() {
//        assertEquals(4, 2 + 2)
//    }
    @Test
    fun test() {
        runBlocking {
            val networkResult = networkOperation()
            val fileResult = fileOperation(networkResult)
            println(fileResult)
        }
        println("-------------")

        networkOperation { networkResult ->
            fileOperation(networkResult) { fileResult ->
                println(fileResult)
            }
        }
        Thread.sleep(1000)
    }

    suspend fun networkOperation() : String = withContext(Dispatchers.IO) {
        //HTTP CALL
        "Hello"
    }

    suspend fun fileOperation(input: String) : String = withContext(Dispatchers.IO) {
        //FILE IO
        "$input World!"
    }

    fun networkOperation(callback: (String) -> Unit) {
        thread {
            //HTTP CALL
            callback("Hello")
        }
    }

    fun fileOperation(input: String, callback: (String) -> Unit) {
        thread {
            //FILE IO
            callback("$input World!")
        }
    }
}