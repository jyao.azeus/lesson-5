package com.example.lesson5

import android.app.Application
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.lesson5.sqlite.AppDatabase
import net.sqlcipher.database.SupportFactory

class App : Application() {

    lateinit var appDatabase: AppDatabase
        private set

    override fun onCreate() {
        super.onCreate()

        val callback = object : RoomDatabase.Callback() {}

        val factory = SupportFactory("passphrase".toByteArray(), null)

        appDatabase = Room.databaseBuilder(
            this,
            AppDatabase::class.java, "local-database"
        ).openHelperFactory(factory)
            .addCallback(callback)
            .build()
    }
}