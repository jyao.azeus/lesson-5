package com.example.lesson5.sqlite

import android.app.Application
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import com.example.lesson5.App
import com.example.lesson5.R
import com.google.android.material.textfield.TextInputEditText
import kotlinx.coroutines.*
import kotlin.concurrent.thread


class MainActivity : AppCompatActivity() {

    private val usersViewModel by viewModels<UsersViewModel>()

    private val logs by lazy { findViewById<TextView>(R.id.logs) }

    private val logsView by lazy { findViewById<TextView>(R.id.logs) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_sqlite_main)

        val firstNameView = findViewById<TextInputEditText>(R.id.firstNameInput)
        val lastNameView = findViewById<TextInputEditText>(R.id.lastNameInput)
        lastNameView.resources.getColor(R.color.black, null)
        findViewById<Button>(R.id.addUser).setOnClickListener {
            usersViewModel.createUser(firstNameView.text.toString(), lastNameView.text.toString())
        }

        findViewById<Button>(R.id.fetchUsers).setOnClickListener {
            fetchAllUsersViaCoroutine()
            //fetchAllUsersViaThread()
        }
    }

    private fun fetchAllUsersViaCoroutine() {
        lifecycleScope.launch {
            val users = usersViewModel.fetchAllUsers()
            logUsers(users)
        }
    }

    private fun fetchAllUsersViaThread() {
        usersViewModel.fetchUsersWithCallback {
            runOnUiThread {
                logUsers(it)
            }
        }
    }

    private fun logUsers(users: List<User>) {
        users.forEach {
            logs.append("${it.firstName} ${it.lastName}")
            logs.append("\n")
        }
        logs.append("-------------\n")
    }
}

class UsersViewModel(app: Application) : AndroidViewModel(app) {

    private val db = (app as App).appDatabase

    fun createUser(firstName: String, lastName: String) {
        viewModelScope.launch {
            val user = User(firstName, lastName)
            withContext(Dispatchers.IO) {
                db.userDao().insert(user)
            }
        }
    }

    suspend fun fetchAllUsers(): List<User> {
        return withContext(Dispatchers.IO) {
            db.userDao().getAll()
        }
    }

    fun fetchUsersWithCallback(callback: (List<User>) -> Unit) {
        thread {
            val users = db.userDao().getAll()
            callback(users)
        }
    }
}