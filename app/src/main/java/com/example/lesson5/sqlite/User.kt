package com.example.lesson5.sqlite

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "USER")
data class User(
    @ColumnInfo(name = "FIRST_NAME") val firstName: String,
    @ColumnInfo(name = "LAST_NAME") val lastName: String,
    @PrimaryKey(autoGenerate = true) val uid: Int? = null)